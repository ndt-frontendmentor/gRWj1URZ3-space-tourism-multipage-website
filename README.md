# Frontend Mentor - Space tourism website solution

[Space tourism website challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/space-tourism-multipage-website-gRWj1URZ3)

## Table of contents

- [Overview](#overview)
  - [TODO](#todo)
  - [Links](#links)
  - [Screenshots](#screenshots)
- [Study Cases](#study-cases)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
- [Author](#author)
- [Acknowledgments](#acknowledgments)
  - [Useful resources](#useful-resources)

## Overview

### TODO

Users should be able to:

- [ ] View the optimal layout for each of the website's pages depending on their
      device's screen size
- [ ] See hover states for all interactive elements on the page
- [ ] View each page and be able to toggle between the tabs to see new
      information

### Links

- [Solution URL](https://gitlab.com/ndt-frontendmentor/5cacc469fec04111f7b848ca-rest-countries-api-with-color-theme-switcher)
- [Live Site URL](https://ndt-frontendmentor.gitlab.io/5cacc469fec04111f7b848ca-rest-countries-api-with-color-theme-switcher)

### Screenshots

![](./screenshot/desktop-home.jpg) ![](./screenshot/desktop-crew.jpg)
![](./screenshot/desktop-dest.jpg) ![](./screenshot/desktop-tech.jpg)
![](./screenshot/mobile-home.jpg) ![](./screenshot/mobile-crew.jpg)
![](./screenshot/mobile-dest.jpg) ![](./screenshot/mobile-tech.jpg)

## Study Cases

### Built with

- Mobile-first workflow
- Lume (powered by Deno)
- JavaScript/TypeScript
- HTML5
- CSS3/SCSS (Sass)

### What I learned

<!-- TODO update later -->

## Author

- Website - [Nguyễn Đăng Tú](https://ngdangtu.com)
- Frontend Mentor -
  [@ngdangtu-vn](https://www.frontendmentor.io/profile/ngdangtu-vn)\
  (Even though I didn't wish to choose this username! There is nothing I can do
  about this 😭)
- Mastodon - [@ngdangtu@tilde.zone](https://tilde.zone/@ngdangtu)

## Acknowledgments

This is where I give a hat tip to anyone helps me or any project that backs my
work.

### Useful Resources

- <!-- TODO update later -->
